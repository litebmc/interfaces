from conan import ConanFile
from conan.tools.cmake import CMake
from conan.tools.cmake import CMakeToolchain
from conan.tools.files import copy
import os
<%
from inflection import underscore
pub_dep = underscore(pkg.interface.replace("DBus", "dbus")) + "-public"
%>

class LitebmcInterfaceConan(ConanFile):
    name = "${pkg.name}"
    version = "${pkg.version}"
    settings = "os", "arch", "compiler", "build_type"
    description = "The ${pkg.build_type} library of interface ${pkg.interface}"
    url = "https://litebmc.com"
    homepage = ""
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "PkgConfigDeps"
    license = "BSL-1.0"
    package_type = "shared-library"
    exports_sources = "CMakeLists.txt", "interface.pc.in"
    exports = "${pkg.interface}.yaml"
    options = {
        "codegen_version": [
% for ver, his in codegen_history.items():
            "${ver}",
% endfor
        ],
    }
    default_options = {
        "codegen_version": "${codegen_version.str}"
    }

    def requirements(self):
% for ver, his in codegen_history.items():
        if self.options.codegen_version == "${ver}":
            self.requires("${his.lb_base}")
% endfor
    % if pkg.build_type != "public":
        self.requires("${pub_dep}/${pkg.version}")
    % else:
    % for intf_name, idf_intf in intf.dependency_idf_interface.items():
        self.requires("${underscore(intf_name.replace("DBus", "dbus"))}-public/[>=${pkg.version} <${str(codegen_version.major + 1)}.${codegen_version.minor}]")
    % endfor
    % endif

    def configure(self):
% for ver, his in codegen_history.items():
        if self.options.codegen_version == "${ver}":
            self.options["lb_base"].compatible_required = "${his.lb_base_compatible_required}"
% endfor
        pass

    def generate(self):
        tc = CMakeToolchain(self)
        tc.variables["CMAKE_INSTALL_INCLUDEDIR"] = "usr/include"
        tc.variables["CMAKE_INSTALL_LIBDIR"] = "usr/lib"
        tc.variables["CMAKE_PROJECT_VERSION"] = self.version
        tc.variables["CMAKE_BUILD_TYPE"] = self.settings.build_type
        tc.variables["CMAKE_INSTALL_DATAROOTDIR"] = "usr/share"
        tc.variables["CODEGEN_VER"] = self.options.codegen_version
        tc.generate()
        # Copy the dependent IDFs
        copy(self, "*.yaml", self.recipe_folder, self.build_folder)
    % for intf_name, idf_intf in intf.dependency_idf_interface.items():
<% indirect_dep = underscore(intf_name.replace("DBus", "dbus")) + "-public" %>\
        % if pkg.build_type == "public":
        copy(self, "*.yaml", self.dependencies["${indirect_dep}"].recipe_folder, self.build_folder)
        % else:
        copy(self, "*.yaml", self.dependencies["${pub_dep}"].dependencies["${indirect_dep}"].recipe_folder, self.build_folder)
        % endif
    % endfor

    def build(self):
        cflags = os.environ.get("CFLAGS", "")
        if self.settings.build_type == "Release" and self.settings.arch == "armv8":
            cflags += " -D_FORTIFY_SOURCE=2"

        cflags += " -fPIC -fpie -fPIE"
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        cmake.install()

    def package_info(self):
        self.cpp_info.includedirs = ["usr/include"]
        self.cpp_info.libdirs = ["usr/lib"]
        self.cpp_info.libs = [self.name]
        self.cpp_info.set_property("cmake_find_mode", "both")
        self.cpp_info.set_property("cmake_target_name", "${pkg.name}:${pkg.name}")
        self.cpp_info.set_property("pkg_config_name", "${pkg.name}")
        self.runenv_info.define("LD_LIBRARY_PATH", os.path.join(self.package_folder, "lib"))
