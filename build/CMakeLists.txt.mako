cmake_minimum_required(VERSION 3.5)
project(${pkg.name} LANGUAGES C VERSION ${pkg.version})
<%
from inflection import underscore
%>\

message(STATUS "generating code start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
message(STATUS "lbk gen -i ${pkg.interface}.yaml")
execute_process(COMMAND lbk gen -i ${pkg.interface}.yaml -t ${pkg.build_type} RESULT_VARIABLE ret)
if(ret AND NOT ret EQUAL 0)
    message(FATAL_ERROR "Generating code failed")
endif()
message(STATUS "Generating code finished <<<<<<<<<<<<<<<<<<<<<<<<<<<<")

include(GNUInstallDirs)

set(CMAKE_C_FLAGS "$\{CMAKE_C_FLAGS} -D__FILENAME__='\"$(notdir $(abspath $(patsubst %.o,%,$@)))\"'")

find_package(PkgConfig REQUIRED)
pkg_check_modules(GLIB2 REQUIRED glib-2.0)
pkg_check_modules(LB_BASE REQUIRED lb_base)
% for dep_intf in intf.dependency_interface:
pkg_check_modules(${dep_intf.split(".")[-1].upper()}_PUB REQUIRED ${underscore(f"{dep_intf}".replace("DBus", "dbus"))}-public)
%endfor
% if pkg.build_type != "public":
pkg_check_modules(PUBLIC_INTF REQUIRED ${underscore(pkg.interface.replace("DBus", "dbus"))}-public)
% endif

aux_source_directory(${pkg.build_type} SRC_LIST)
list(SORT SRC_LIST)

add_library($\{PROJECT_NAME} SHARED $\{SRC_LIST})
target_include_directories($\{PROJECT_NAME} PUBLIC
    $\{GLIB2_INCLUDE_DIRS}
    $\{PUBLIC_INTF_INCLUDE_DIRS}
    $\{LB_BASE_INCLUDE_DIRS}
% for dep_intf in intf.dependency_interface:
    $\{${dep_intf.split(".")[-1].upper()}_PUB_INCLUDE_DIRS}
% endfor
    .
)
target_link_libraries($\{PROJECT_NAME} PUBLIC
    $\{LB_BASE_LIBRARIES}
    $\{PUBLIC_INTF_LIBRARIES}
% for dep_intf in intf.dependency_interface:
    $\{${dep_intf.split(".")[-1].upper()}_PUB_LIBRARIES}
% endfor
)
target_link_directories($\{PROJECT_NAME} PUBLIC
    $\{LB_BASE_LIBRARY_DIRS}
    $\{PUBLIC_INTF_LIBRARY_DIRS}
% for dep_intf in intf.dependency_interface:
    $\{${dep_intf.split(".")[-1].upper()}_PUB_LIBRARY_DIRS}
% endfor
)
SET_TARGET_PROPERTIES($\{PROJECT_NAME} PROPERTIES VERSION $\{CMAKE_PROJECT_VERSION} SOVERSION $\{CMAKE_PROJECT_VERSION_MAJOR})

configure_file(
    interface.pc.in
    .temp/${pkg.name}.pc
    @ONLY
)
install(
    FILES .temp/${pkg.name}.pc
    DESTINATION $\{CMAKE_INSTALL_LIBDIR}/pkgconfig
)

install(TARGETS $\{PROJECT_NAME} DESTINATION $\{CMAKE_INSTALL_LIBDIR})
install(FILES ${pkg.build_type}/${pkg.interface}.h DESTINATION $\{CMAKE_INSTALL_INCLUDEDIR}/${pkg.build_type})
% if pkg.build_type == "public":
install(FILES ${pkg.build_type}/${pkg.interface}.xml DESTINATION $\{CMAKE_INSTALL_DATAROOTDIR}/dbus-1/interfaces/)
% endif
% if pkg.build_type == "server":
install(FILES ${pkg.build_type}/schema/${pkg.interface}.json DESTINATION $\{CMAKE_INSTALL_DATAROOTDIR}/litebmc/schema/)
% endif
