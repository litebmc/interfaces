prefix=@CMAKE_INSTALL_PREFIX@
exec_prefix=$\{prefix}
libdir=$\{prefix}/@CMAKE_INSTALL_LIBDIR@
includedir=$\{prefix}/@CMAKE_INSTALL_INCLUDEDIR@

Name: @PROJECT_NAME@
Description: The library for dbus interface: ${pkg.interface}.xml
Version: @CMAKE_PROJECT_VERSION@
Requires:
Libs: -L$\{libdir} -l${pkg.name}
Cflags: -I$\{includedir}