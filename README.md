# 说明

`interfaces.git`仓记录了litebmc申明或使用的dbus接口，接口文件称这`idf`文件。

`build.py`脚本会调用 **lbkit提供的接口代码自动生成工具** 将idf文件转换成C代码，随后构建出名为public、client和server的conan包，这些包将被推送到conan中心仓供组件使用。组件实际依赖的就是一个个接口conan包。

# idf

idf文件使用Yaml格式描述，内容格式由lbkit提供/usr/share/litebmc/schema/idf.v`X`.json文件(json schema)定义，其中`X`表示接口版本号，当前为`1`。

如果使用vscode，建议安装`redhat.vscode-yaml`插件，该插件提供了错误检查、配置建议、自动补全等功能

## idf文件内容

idf文件描述了接口版本号、描述、别名、标记、属性、方法、信号、结构体、枚举、字典和插件，基本格式示例：

```yaml
# yaml-language-server: $schema=/usr/share/litebmc/schema/idf.v1.json
version: 2
alias: Connector
description: 连接器接口，用于加载下层对象
flags: ""
properties: []
methods: []
signals: []
structures: []
enumerations: []
dictionaries: []
```

### yaml-language-server

yaml-language-server描述了当前IDF文件应该遵循的idf文件版本号，将来会基于此版本号扩展新功能。

### version 版本号

idf文件的版本号由两位数字组成，需要满足正则表达式`^(0|([1-9][0-9]*))\\.(0|([1-9][0-9]*))$`要求。自动生成工具还有一个工具版本号用于表征生成代码的兼容性，最终生成的conan包版本号结合了工具版本号和接口版本号生成一个三段式版本号。如接口版本号为`0.2`，工具兼容性版本号为`3`,则生成的conan包版本号为`3.0.2`。

### description 描述

接口简要描述，用于开发者快速了解该接口提供的服务。

### alias 别名

dbus接口较长，别名用于简化C代码的结构体申明，如`com.litebmc.Connector.yml`的接口C代码会生成名为`Connector`的结构体表示对象。

如果组件使用的多个接口申明了同一个别名就会产生类型冲突，导致编译失败。因此，。

管理原则：

* 别名需要在interfaces仓中唯一。
* 别名需要有意义。
* 别名采用“先占原则”管理，也就是首先上库的接口拥有别名。
* 别名不能与基础库、常见库中已申明的类型冲突。

### flags 标记

支持`deprecated`废弃标记。

### properties 属性

每个idf接口都可以有零个或无数个属性，每个属性都可以申明`name`、`type`、`description`、`flags`等，其中`name`和`type`为必选项。

示例:

```yaml
properties:
   - name: b
     description: b
     type: boolean
     flags: readonly,hidden
     default: true
   - name: ab
     description: ab
     type: array[boolean]
     flags: readonly
     default: [true, false]
   - name: i
     description: i
     type: int32
     flags: readwrite
     max: 100000
     default: 0x128
   - name: ai
     description: ai
     type: array[int32]
     max: 128
     flags: readwrite
     default: [0, 100]
```

#### name 属性名

属性名需要满足正则表达式要求`^[A-Za-z][a-zA-Z0-9_]*$`

#### type 属性类型

值类型包括布尔、数字、字符串、容器、数组、结构体、枚举、字典、列表几类：
1. 布尔: `boolean`
2. 数字: `byte|uint16|uint32|uint64|int16|int32|int64|size|ssize|double|unixfd`
3. 字符串：`string|object_path|signature`
4. 容器: `variant`
5. 结构体: `struct[<intf>.<name>]`，其中`<name>`为结构体类型名，`<intf>`为接口名称
6. 字典: `dict[<intf>.<name>]`，其中`<name>`为字曲类型名，`<intf>`为接口名称
7. 枚举: `enum[<intf>.<name>]`，其中`<name>`为枚举类型名，见枚举章节说明
8. 列表: `set[enum[<intf>.<name>]]`，其中`<name>`为枚举类型名，当前set类型在内部实现就是数组，因此等效为`array[enum[<intf>.<name>]]`
9. 数组: `array[<name>]`，其中`<name>`可以是布尔、数字、字符串、结构体、枚举、字典，注意不支持数组、容器和列表

* struct、dict、enum可以使用`<intf>`申明类型来源的接口，当intf为`self.Unit`表示使用当前接口定义的Unit类型；或者`com.litebmc.Sensor.Unit`由表示使用`com.litebmc.Sensor`接口定义的Unit类型。

#### description 简要说明

可选的简要说明

#### flags 属性标记

支持申明多个以`,`逗号分隔的标记，每个标记都会作用于不同阶段:

* `readonly|writeonly|readwrite`: 运行时标记属性是否允许读写，分别对应dbus的`access`的`read`、`write`和`readwrite`，未申明时为`readwrite`。
* `deprecated`: 编译时标记属性已废弃，生成的C代码会打上`__attribute__((__deprecated__))`标记，标记作用于编译时。
* `hidden`: 运行时标记为私有属性，此属性无法在dbus总线上读写
* `emits_change|emits_invalidation|const|emits_false`: 运行时标记属性值是否变化以及变化时的是否发布信号，对应dbus的`org.freedesktop.DBus.Property.EmitsChangedSignal`注释的true、invalidation、const、false值，仅可申明其中某一个，未申明时默认为`emits_change`
* `refobj`: 作用于odf加载或odf打包时校验数据合法性，表示属性引用当前进程的其它对象，属性值可以是`object_path`、`${<interface>:<object_path>}`或`${<interface>:<object_path>.<property_name>}`
* `required`: 作用于odf加载或odf打包时校验属性配置是否缺失

#### max、min、exclusive_max、exclusive_min 取值范围约束

如果属性类型为数字，这四个属性可以标记取值的最大值（val<=max）、最小值(val>=min)、极大值(`val<exclusive_max`)和极小值(`val>exclusive_min`)。odf加载或odf打包时校验属性值是否满足要求，这四个值的类型以及可取值范围都与type对应，例如byte类型的取值值必须是数字且最大为255，最小为0。

#### pattern 字符串正则表达式约束

如果属性类型为字符串，用于标记属性值应该满足的正则表达式。

#### default 属性默认值

标记odf未申明的属性的默认值，默认值的类型以及取值范围都与属性type类型对应

### methods 方法

每个接口可以申明零个或多个方法，方法可以申明`name`方法名、`description`方法简要说明、`flags`方法标记、`parameters`入参和`returns`出参，其中`name`为必选项。

示例:

```yaml
methods:
   - name: SetBool
     description: 设置bool值方法
     flags: deprecated
     parameters:
      -  name: in
         description: in
         type: boolean
      -  name: in_array
         description: in_array
         type: array[boolean]
     returns:
      -  name: out
         description: out
         type: boolean
      -  name: out_array
         description: out_array
         type: array[boolean]
```

#### name 方法名

方法名需要满足正则表达式要求`^[A-Z]((_[A-Z])|([A-Za-z0-9]))*$`，要求为大驼峰命名，可以使用`_`连接多个单词，如`SetPowerOn`、`Set_Power_On`

#### description 简要说明

可选的简要说明

#### parameters 入参和 returns 出参

每个方法可以有零个或多个入参以及零个或多个出参，入参和出参都是数组类型，参数是有序的，在调用方法或方法实现时会按顺序传递或返回参数。

每个出入参都有`name`、`type`、`description`三个成员，其中`name`和`type`为必选项：

1. name: 参数名需要满足正则表达式`^[A-Za-z][a-zA-Z0-9_-]*$`要求。
2. type: 参数类型与`properties`的`type`一致。
3. description: 参数简要说明。

#### flags 方法标记

支持`deprecated`废弃标记。

### signals 信号

每个接口可以申明零个或多个信号，信号的成员定义与方法同名成员定义相同，可以申明`name`方法名、`description`方法简要说明、`flags`信号标记、`properties`四个成员，其中`name`为必选项,`properties`定义与`methods`方法的出入参一致。

示例:

```yaml
signals:
   - name: TestSignal
     description: 测试信号
     flags: deprecated
     properties:
      -  name: Enabled1
         description: Enabled1
         type: int32
      -  name: Enabled2
         description: Enabled2
         type: uint32
      -  name: Enabled3
         description: Enabled3
         type: int64
```

### enumerations 枚举

接口可以申明零个或多个枚举类型，每个枚举申明时可以有`name`、`description`、`flags`、`values`，其中`name`和`description`为必选项。

#### name 枚举名

必须使用大驼峰格式，正则表达式为`[A-Z]((_[A-Z])|([A-Za-z0-9]))*$`。

#### flags 枚举标记

支持`deprecated`废弃标记。

#### description 枚举说明

可选的简要说明

#### values 枚举值列表

使用values数组记录枚举成员，values数组至少1个成员，每个成员都必须填写`name`和`description`，其中`name`需要满足下则表达式`^[A-Z][a-zA-Z0-9_-]*$`。

### structures 结构体

接口可以申明零个或多个结构体，每个结构体可以有`name`、`description`、`flags`、`values`，其中`name`为必选项。

示例：

```yaml
structures:
  - name: StructArrayStrThenStr
    description: (ass)
    values:
      - name: as
        description: as
        type: array[string]
      - name: s
        description: s
        type: string
```

#### name 结构体名

必须使用大驼峰格式，正则表达式为`[A-Z]((_[A-Z])|([A-Za-z0-9]))*$`。

#### flags 标记

支持`deprecated`废弃标记。

#### description 简要说明

可选的简要说明

#### values 结构体成员

结构体成员定义与`methods`的出入参一致。

### dictionaries 字典

接口可以申明零个或多个字典，每个字典可以有`name`、`description`、`flags`、`values`、`key`、`key_type`，其中`name`、`key`、`value`为必选项。

示例:

```yaml
dictionaries:
   - name: DictStr
     key: Name
     description: 字典
     values:
      - name: Str
        description: s
        type: string
```

自动生成工具将生成一个`DictStr`结构体，并生成操作字典所需的查询、插入、删除、检查是否包含、清空和循环处理成员等函数，示例:

```c
typedef void (*DictStr_func)(const gchar *key, DictStrName *value, gpointer user_data);
struct _DictStr {
    /* the ownership NOT transferred */
    DictStrName *(*lookup)(const DictStr *dict, const gchar *key);
    /* if return TRUE, ownership of `value` is transferred to the dict */
    gboolean (*insert)(const DictStr *dict, const gchar *key, DictStrName **value);
    gboolean (*remove)(const DictStr *dict, const gchar *key);
    gboolean (*contains)(const DictStr *dict, const gchar *key);
    void (*clear)(const DictStr *dict);
    void (*foreach)(const DictStr *dict, DictStr_func func, gpointer user_data);
};
/* Create a new DictStr object */
DictStr *DictStr_new(void);
```

* 注意：操作字典是非多线程安全的，因此需要业务负责加锁保护。

#### name 字典名

必须使用大驼峰格式，正则表达式为`[A-Z]((_[A-Z])|([A-Za-z0-9]))*$`。

#### flags 标记

支持`deprecated`废弃标记。

#### description 简要说明

可选的简要说明

#### key 和 key_type 字典索引

索引名采用大驼峰命名风格，正则表达式为`^[A-Z]((_[A-Z])|([A-Za-z0-9]))*$`,索引类型可以是非浮点数、布尔的基础类型以及枚举，正则表达式为`^(byte|uint16|uint32|uint64|int16|int32|int64|size|ssize|string|object_path|signature|unixfd|(enum\\[[a-zA-Z][A-Za-z0-9_]*(\\.[a-zA-Z][A-Za-z0-9_]*)*\\]))$`

#### values 成员

字典成员定义与`methods`的出入参基本一致，唯一差异是字典成员至少1个

### plugins 插件

每个接口还可以申明零个或多个插件事件（`action`），由`plugins`对象的`actions`记录接口支持事件清单，每个action支持`name`、`description`、`policy`、`flags`、`parameters`和`returns`组成。

litebmc的插件实际是一个动态库，插件需要申明一个`plugin_start`启动函数，在该函数中需要完成插件事件的钩子函数注册，随后等待事件发生时调用钩子函数。

基于lb_core开发的litebmc服务启动时，会尝试从app可执行文件所在目录下的`plugins`目录或`/opt/litebmc/plugins`查找插件，加载插件时会时会调用动态库提供的`plugin_start`函数，完成事件注册。

com.litebmc.Connector接口申明了一个OdfValidate事件，示例:

```yaml
plugin:
  actions:
    - name: OdfValidate
      description: 测试多个接口执行所有接口
      policy: "return_any_fail"
      parameters:
        - name: odf_file
          description: 待验证ODF文件
          type: string
```

上述示例申明了一个OdfValidate事件，自动生成工具将在接口的`public`包中生成事件注册、注销和执行的函数：

```c
// 插件实现方调用此方法注册action钩子
int Connector_OdfValidate_register(const gchar *req_signature, const gchar *rsp_signature,
    Connector_OdfValidate_action action, gpointer user_data);
// 插件实现方调用此方法注册钩子
void Connector_OdfValidate_unregister(Connector_OdfValidate_action action);
// 插件调用方在特定位置调用此函数，按注册顺序依次调用插件action钩子函数
int Connector_OdfValidate_run(const Connector *object, const Connector_OdfValidate_Req *req);
```

访插件涉及`lb_core`的`service/connector.service.c`插件动作运行逻辑以及[lb_odf_validate](https://gitee.com/litebmc/lb_odf_validate.git)插件实现。
主要的处理流程包括：
1. 应用启动时查询plugins目录，调用lb_odf_validate提供的plugin_start函数。
2. plugins_start函数调用Connector_OdfValidate_register注册插件动作钓子
3. connector.service.c加载odf文件时，调用Connector_OdfValidate_run在特定位置执行插件动作，最终调用到lb_odf_validate注册的钓子函数，由该函数完成事件处理并返回处理结果以及响应体。

#### name 字典名

必须使用大驼峰格式，正则表达式为`[A-Z]((_[A-Z])|([A-Za-z0-9]))*$`。

#### flags 标记

支持`deprecated`废弃标记。

#### description 简要说明

可选的简要说明

#### policy

事件执行策略，可选分别为`return_any_success`任一成功退出、`return_any_fail`任一失败退出、`continue_always`继续执行，默认为继续执行。

#### parameters入参和values出参

成员定义与`methods`的出入参一致。


# 构建

接口文件不直接用于组件编程，而是由自动生成工具转换成C代码后构建出名为public、client和server的conan包，这些包将被推送到conan中心仓供组件使用。

接口文件的version申明了两段式版本号，自动生成工具还有一个工具版本号用于表征自己的兼容性版本，最终生成的conan包版本号结合工具版本号和接口版本号生成一个三段式版本号。如接口版本号为`0.2`，工具兼容性版本号为`3`,则生成的conan包版本号为`3.0.2`。

接口的conan包名格式为`<lower(name)>-<public|server|client>`，如com.litebmc.Connector.yml接口生成的包名为`com.litebmc.connector-public`、`com.litebmc.connector-server`和`com.litebmc.connector-client`。

构建出包时可以指定conan包的user和channel，默认生成的conan包为`<name>-<public|server|client>/<version>`，也可以使用`-U`和`-c`指定user和channel生成conan包`<name>-<public|server|client>/<version>@<user>/<channel>`。

命令参数说明:

```shell
$ ./build.py --help
usage: build.py [-h] [-i INTERFACE] [-cv CODEGEN_VERSION] [-u] [-r REMOTE] [-c CHANNEL] [-U USER]

Create conan packages for each interface

options:
  -h, --help            show this help message and exit
  -i INTERFACE, --interface INTERFACE
                        Only build special interface, default: all
  -cv CODEGEN_VERSION, --codegen_version CODEGEN_VERSION
                        Codegen version of lbkit
  -u, --upload          upload package
  -r REMOTE, --remote REMOTE
                        Conan仓别名(等同conan的-r选项)
  -c CHANNEL, --channel CHANNEL
                        package channel, default: None, see detail: https://docs.conan.io/en/1.20/reference/conanfile/attributes.html#user-channel
  -U USER, --user USER  package user, default: None, see detail: https://docs.conan.io/en/1.20/reference/conanfile/attributes.html#user-channel
```

示例1：`-i`参数指定com.litebmc.Connector.yaml接口，命令生成三个名为`com.litebmc.connector-public/3.0.2`、`com.litebmc.connector-server/3.0.2`和`com.litebmc.connector-client/3.0.2`conan包。

```shell
$ ./build.py -i com/litebmc/Connector.yaml
['-i', '/home/xuhj/workspace/litebmc/interfaces/.temp/com.litebmc.Connector/com.litebmc.Connector.yaml', '-d', '.', '-cv', '3']
The sha256sum of interface /home/xuhj/workspace/litebmc/interfaces/.temp/com.litebmc.Connector/public/com.litebmc.Connector.xml is 2b0fe6c6a45b67223f88ac01abc75f5f2a0eb087e33081629f2b7ce0ea80c1b4
>> Start create pacakge com.litebmc.connector-public/3.0.2
>>>> (com.litebmc.connector-public/3.0.2) conan create . -r litebmc -o lb_base/*:shared=True --build=missing
>> Start create pacakge com.litebmc.connector-server/3.0.2
>>>> (com.litebmc.connector-server/3.0.2) conan create . -r litebmc -o lb_base/*:shared=True --build=missing
>> Start create pacakge com.litebmc.connector-client/3.0.2
>>>> (com.litebmc.connector-client/3.0.2) conan create . -r litebmc -o lb_base/*:shared=True --build=missing
```

示例2：`-i`参数指定com.litebmc.Connector.yaml接口，命令生成三个名为`com.litebmc.connector-public/3.0.2@litebmc/stable`、`com.litebmc.connector-server/3.0.2@litebmc/stable`、`com.litebmc.connector-client/3.0.2@litebmc/stable`的conan包。

```shell
$ ./build.py -U litebmc -c stable -i com/litebmc/Connector.yaml 
['-i', '/home/xuhj/workspace/litebmc/interfaces/.temp/com.litebmc.Connector/com.litebmc.Connector.yaml', '-d', '.', '-cv', '3']
The sha256sum of interface /home/xuhj/workspace/litebmc/interfaces/.temp/com.litebmc.Connector/public/com.litebmc.Connector.xml is 2b0fe6c6a45b67223f88ac01abc75f5f2a0eb087e33081629f2b7ce0ea80c1b4
>> Start create pacakge com.litebmc.connector-public/3.0.2@litebmc/stable
>>>> (com.litebmc.connector-public/3.0.2@litebmc/stable) conan create . -r litebmc -o lb_base/*:shared=True --build=missing
>> Start create pacakge com.litebmc.connector-server/3.0.2@litebmc/stable
>>>> (com.litebmc.connector-server/3.0.2@litebmc/stable) conan create . -r litebmc -o lb_base/*:shared=True --build=missing
>> Start create pacakge com.litebmc.connector-client/3.0.2@litebmc/stable
>>>> (com.litebmc.connector-client/3.0.2@litebmc/stable) conan create . -r litebmc -o lb_base/*:shared=True --build=missing
```

示例3：不指定`-i`参数，将默认构建出所有组件包，此命令会随机构建单个接口以完成接口依赖构建，随后启动多个进程构建其它接口。

## ChangeLog

#### 2024.12.12

1. 结构体、枚举、字典支持引用其它接口申明的类型，如`enum[com.litebmc.Sensor.Unit]`可以在当前接口中使用`com.litebmc.Sensor`接口申明的Unit枚举。
2. 跨接口引用将导致接口间依赖，本次变更调整`build.py`构建逻辑以支持自动构建被依赖接口，同时确保多进程并行构建时不因依赖关系不满足导致构建失败。
