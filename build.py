#!/usr/bin/python3

import os
import sys
import shutil
import argparse
import yaml
import time
import multiprocessing
from inflection import underscore
from multiprocessing import Process
from mako.lookup import TemplateLookup
from lbkit.codegen.codegen import CodeGen
from lbkit.tools import Tools
from lbkit.misc import SmartFormatter
from lbkit.codegen.codegen import __version__ as codegen_version
from lbkit.codegen.codegen import history_versions as codegen_history
from lbkit.codegen.codegen import version_check
from lbkit.codegen.codegen import Version
from lbkit.codegen.codegen import codegen_version_arg

tools = Tools("interfaces_build")
log = tools.log
cwd = os.getcwd()


class PkgInfo(object):
    """生成组件基本信息，包括版本号，组件名等"""

    def __init__(self, package_yml, build_type):
        self.build_type = build_type
        with open(package_yml, "r", encoding="utf-8") as fp:
            data = yaml.load(fp, yaml.FullLoader)
            # 默认使用1.0.x表示接口的版本号，其中 x是接口的版本号，必须向下兼容
            self.version = "1.0." + str(data.get("version"))
            self.interface = data.get("name")
            self.name = underscore(f"{self.interface}".replace("DBus", "dbus")) + f"-{self.build_type}"


class InterfaceBuild(Process):
    """构建接口，生成public/server/client三个组件"""
    def __init__(self, need_upload, remote, intf_file, codegen_version: Version, lock, states: dict[str, int]):
        super().__init__()
        self.need_upload =  need_upload
        self.remote = remote
        self.codegen_version: Version = codegen_version
        self.lock = lock
        self.states = states
        build_path = intf_file[:-5].replace("/", ".").strip('.')
        self.src_intf_file = os.path.realpath(intf_file)
        self.dst_intf_file = intf_file.replace('/', '.').strip('.')
        self.build_path = os.path.join(cwd, ".temp", build_path)
        self.tools = Tools(os.path.basename(self.dst_intf_file), log_dir=self.build_path)
        self.log = self.tools.log

    def run(self):
        """构建任务"""
        cwd = os.getcwd()
        while True:
            self.lock.acquire()
            state = self.states.get(self.src_intf_file)
            if state is None:
                # 如果未构建，则设置为正在构建
                self.states[self.src_intf_file] = 0
                self.lock.release()
                break
            elif state == 0:
                # 如果状态已正在构建，则等待1秒后再检查
                self.lock.release()
                time.sleep(1)
            else:
                self.lock.release()
                return
        shutil.rmtree(self.build_path, ignore_errors=True)
        os.makedirs(self.build_path)
        os.chdir(self.build_path)
        shutil.copyfile(self.src_intf_file, self.dst_intf_file)
        gen_args = ["-i", self.dst_intf_file, "-d", ".", "-cv", f"{self.codegen_version.str}"]
        gen = CodeGen(gen_args)
        gen.run()
        idf_intf = gen.get_interface(self.dst_intf_file)
        # 如果一个接口依赖其它组件，则先构建依赖
        for dep_intf in idf_intf.dependency_interface:
            os.chdir(cwd)
            intf_file = dep_intf.replace(".", "/") + ".yaml"
            intf = InterfaceBuild(self.need_upload, self.remote, intf_file, self.codegen_version, self.lock, self.states)
            intf.run()
        os.chdir(self.build_path)
        for build_type in ["public", "server", "client"]:
            os.chdir(self.build_path)
            pkg = PkgInfo("package.yml", build_type)
            pkg_name = f"{pkg.name}/{pkg.version}"
            lookup = TemplateLookup(directories=os.path.join(cwd, "build"))
            for template in ["conanfile.py.mako", "CMakeLists.txt.mako", "interface.pc.in.mako"]:
                mako = lookup.get_template(template)
                outfile = template[:-5]
                with open(f"{build_type}/{outfile}", "w", encoding="utf-8") as fp:
                    out = mako.render(pkg=pkg, codegen_version=self.codegen_version, intf=idf_intf, codegen_history=codegen_history)
                    if outfile == "CMakeLists.txt":
                        out = out.replace("$\{", "${")
                    if outfile == "interface.pc":
                        out = out.replace("$\{", "${")
                    fp.write(out)
            os.chdir(build_type)
            shutil.copyfile(self.src_intf_file, pkg.interface + ".yaml")
            self.log.info(f">> Start create pacakge {pkg_name}")
            verbose = True if os.environ.get("VERBOSE") else False
            verbose = True if os.environ.get("SHOW_LOG") else verbose
            os.environ["CODEGEN_VERSION"] = self.codegen_version.str
            cmd = f"conan remove {pkg.name}/{pkg.version} -c"
            self.tools.exec(cmd)
            cmd = f"conan create . -r {self.remote} -o lb_base/*:shared=True --build={pkg.name}/* --build=missing"
            self.tools.exec(cmd, log_prefix=pkg_name, verbose=verbose)
            if self.need_upload:
                self.log.info(f">> Start upload pacakge {pkg_name}")
                cmd= f"conan upload {pkg_name} -r {self.remote} --only-recipe"
                self.tools.exec(cmd, log_prefix=pkg_name)

        self.lock.acquire()
        # 构建完成
        self.states[self.src_intf_file] = 1
        self.lock.release()

class Build():
    """构建接口，生成public/server/client三个组件"""
    def __init__(self, need_upload, remote, codegen_version: Version):
        self.need_upload =  need_upload
        self.remote = remote
        self.processes = []
        self.codegen_version: Version = codegen_version
        self.lock = multiprocessing.Lock()
        # 记录接口构建状态，key为接口名，如果不存在则未构建，值0表示正在构建，值1表示构建完成
        self.states: dict[str, int] = multiprocessing.Manager().dict()

    def __create_all_interface(self):
        # 为方便调试，首个接口串行构建，之后的才并行构建
        first_build = True
        for root, _, files in os.walk(".", topdown=False):
            # 接口描述文件至少两层目录，另外忽略隐藏目录下的文件
            if root.startswith('./.') or len(root.split("/")) < 3:
                continue
            for file in files:
                file = os.path.join(root, file)
                if file.endswith(".yaml") and os.path.isfile(file):
                    process = InterfaceBuild(self.need_upload, self.remote, file, self.codegen_version, self.lock, self.states)
                    if first_build:
                        process.start()
                        while True:
                            if process.is_alive():
                                time.sleep(0.1)
                                continue
                            if process.exitcode != 0:
                                log.error("Build component %s failed.", process.src_intf_file)
                                sys.exit(-1)
                            break

                        first_build = False
                    else:
                        self.processes.append(process)
                        process.start()

    def create(self, intf_file):
        """创建接口组件"""
        if intf_file == "all":
            self.__create_all_interface()
        else:
            os.environ["SHOW_LOG"] = "on"
            process = InterfaceBuild(self.need_upload, self.remote, intf_file, self.codegen_version, self.lock, self.states)
            self.processes.append(process)
            process.start()
        while True:
            time.sleep(0.1)
            finished = True
            for proc in self.processes:
                if proc.is_alive():
                    finished = False
                elif proc.exitcode != 0:
                    log.error("Build component %s failed.", proc.dst_intf_file)
                    # 强制关键其它进程
                    for proc in self.processes:
                        log.error("Kill build thread %s.", proc.dst_intf_file)
                        proc.terminate()
                    sys.exit(-1)
            if finished:
                return

def check_alias():
    for root, _, files in os.walk(".", topdown=False):
        # 接口描述文件至少两层目录，另外忽略隐藏目录下的文件
        if root.startswith('./.') or len(root.split("/")) < 3:
            continue
        for file in files:
            file = os.path.join(root, file)
            if not file.endswith(".yaml") or not os.path.isfile(file):
                continue
        names = []
        with open(file, "r", encoding="utf-8") as fp:
            idf = yaml.load(fp, yaml.FullLoader)
            alias = idf.get("alias")
            if not alias:
                raise Exception("property alias is missing")
            if alias in names:
                raise Exception(f"interface name {alias} already exists")
            names.append(names)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Create conan packages for each interface", formatter_class=SmartFormatter)
    parser.add_argument(
        "-i", "--interface", help='Only build special interface, default: all', default="all")
    codegen_version_arg(parser)
    parser.add_argument(
        "-u", "--upload", help='upload package', action="store_true")
    parser.add_argument(
        "-r", "--remote", default="litebmc", help="Conan仓别名(等同conan的-r选项)")
    args = parser.parse_args()
    interface = args.interface
    if interface != "all":
        if not os.path.isfile(interface):
            if not os.path.isfile(interface + ".yaml"):
                log.error(f"interface file {interface} not exist")
                sys.exit(-1)
            else:
                interface += ".yaml"
    check_alias()
    version_check(args.codegen_version)
    ver = Version(args.codegen_version)
    ib = Build(args.upload, args.remote, ver)
    ib.create(interface)
